This project documents a relatively simple configuration to use sbuild
unshare mode, allowing package building in a chroot without root
privileges.

It also uses sbuild hooks to perform a second build within the
environment using reprotest, which can change some aspects of the
build environment to detect and troubleshoot reproducibility issues.

To get started:

    sudo apt install mmdebstrap sbuild zstd

As a regular user:

    ./mm-sbuild
    # get a debian source package, such as hello
    apt source hello
    cd hello-*/
    SBUILD_CONFIG=../sbuild-reprotest.conf sbuild -d UNRELEASED -c sid-amd64

Alternately, you can also try with the "notveryreproducible" package,
which intentionally embeds aspects of the build environment into the
package:

    ./mm-sbuild
    git clone https://salsa.debian.org/reproducible-builds/notveryreproducible.git
    cd notveryreproducible
    SBUILD_CONFIG=../sbuild-reprotest.conf sbuild -d UNRELEASED -c sid-amd64

... which should fail the build every time and present you with
gorgeous plain-text diffoscope output!

Occasionally, mm-sbuild will need to periodically be run in order to
freshen up the tarball that sbuild uses to create the chroot.


To also check for usrmerge differences, tell mm-sbuild to make a
tarball without usrmerge:

    USRMERGE=false ./mm-sbuild

And tell sbuild to use the nousermerge tarball instead:

    SBUILD_CONFIG=../sbuild-reprotest.conf sbuild -d UNRELEASED -c sid-amd64-nousermerge


Not all features of reprotest are well supported. The variations for
build_paths cannot be effectively turned off, and fileordering is
unsupported. The user_group and domain_host variations are implemented
outside of reprotest and are not easy to disable or enable on the
fly. Trying to use the --auto-build feature of reprotest does not use
the build performed by sbuild to compare against.It currently requires
manually modifying sbuild-reprotest.conf to change what variations are
enabled or disabled. Some things could be improved with a wrapper
script to generate the sbuild-reprotest.conf on the fly.


This is part of the Reproducible Builds project, and largely leverages
mmdebstrap, sbuild, reprotest and diffoscope:

https://reproducible-builds.org
https://diffoscope.org
https://salsa.debian.org/reproducible-builds/reprotest
https://salsa.debian.org/debian/sbuild
https://gitlab.mister-muffin.de/josch/mmdebstrap/
